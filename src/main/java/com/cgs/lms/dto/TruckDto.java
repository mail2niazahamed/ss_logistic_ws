package com.cgs.lms.dto;

import java.io.Serializable;

import com.cgs.lms.domain.Trucks;
import com.cgs.lms.domain.Users;

public class TruckDto implements Serializable {

	private Trucks trucks;
	private UsersDto users;
	private CustomersDto customers;

	public Trucks getTrucks() {
		return trucks;
	}

	public void setTrucks(Trucks trucks) {
		this.trucks = trucks;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public CustomersDto getCustomers() {
		return customers;
	}

	public void setCustomers(CustomersDto customersDto) {
		this.customers = customersDto;
	}

}
