package com.cgs.lms.dto;
// Generated Jun 21, 2016 2:25:19 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;

/**
 * OrgUsers generated by hbm2java
 */
public class OrgUsersDto implements java.io.Serializable {

	private String email;
	private String pwd;
	private String firstname;
	private String lastname;
	private String createdby;
	private Date createddt;
	private Integer customerId;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddt() {
		return createddt;
	}

	public void setCreateddt(Date createddt) {
		this.createddt = createddt;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

}
