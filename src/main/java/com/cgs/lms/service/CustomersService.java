package com.cgs.lms.service;

import org.springframework.stereotype.Service;

import com.cgs.lms.dto.CustomersDto;

@Service
public interface CustomersService {

	public CustomersDto getCustomersDto(int customerId);

}
