package com.cgs.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cgs.lms.domain.OrderAudit;

@Service
public interface OrderAuditService {

	List<OrderAudit> getOrderAudit(int parseInt);

}
