package com.cgs.lms.service;

import org.springframework.stereotype.Service;

import com.cgs.lms.domain.OrgUsers;
import com.cgs.lms.dto.LoginUsersDto;
import com.cgs.lms.dto.UsersDto;

@Service
public interface UsersService {

	public UsersDto getUser(int userId);

	public LoginUsersDto authenticate(String email, String pwd);

	public String getOrgRoleName(int orgUsersId);

	public int getOrgRoleDetailId(int orgUsersId);

	public OrgUsers updatePassword(String email, String newpassword);

	public OrgUsers createOrgusers(OrgUsers orgUsers);

	public OrgUsers checkUserAlreadyExists(OrgUsers orgUsers);

}
