package com.cgs.lms.service.impl;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cgs.lms.domain.OrgRoles;
import com.cgs.lms.domain.OrgUsers;
import com.cgs.lms.domain.OrgUsersRoles;
import com.cgs.lms.domain.Users;
import com.cgs.lms.domain.repository.OrgRolesRepository;
import com.cgs.lms.domain.repository.OrgUsersRepository;
import com.cgs.lms.domain.repository.OrgUsersRolesRepository;
import com.cgs.lms.domain.repository.UserRepository;
import com.cgs.lms.dto.LoginUsersDto;
import com.cgs.lms.dto.OrgUsersDto;
import com.cgs.lms.dto.UsersDto;
import com.cgs.lms.mapper.OrgUsersMapper;
import com.cgs.lms.mapper.UsersMapper;
import com.cgs.lms.service.OrderWorkflowService;
import com.cgs.lms.service.UsersService;

@Component("UsersServiceImpl")
public class UsersServiceImpl implements UsersService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	OrgUsersRepository orgUsersRepository;

	@Autowired
	OrgRolesRepository orgRolesRepository;

	@Autowired
	OrgUsersRolesRepository orgUsersRolesRepository;

	@Autowired
	@Qualifier("OrderWorkflowServiceImpl")
	private OrderWorkflowService orderWorkflowService;

	@Inject
	private UsersMapper usersMapper;

	@Inject
	private OrgUsersMapper orgUsersMapper;

	@Override
	public UsersDto getUser(int userId) {
		Users users = userRepository.findOne(userId);
		if (users == null)
			return null;
		return usersMapper.usersToUsersDto(users);
	}

	@Override
	public LoginUsersDto authenticate(String email, String password) {
		// TODO Auto-generated method stub
		OrgUsers orgUsers = orgUsersRepository.findAllByEmailAndPwd(email, password).get(0);

		OrgUsersRoles orgUsersRoles = orgUsersRolesRepository.findOneByOrgUsersId(orgUsers.getId());

		OrgRoles orgRoles = orgRolesRepository.getOne(orgUsersRoles.getOrgRolesId());

		LoginUsersDto orgUsersDto = new LoginUsersDto();
		orgUsersDto.setFirstName(orgUsers.getEmail());
		orgUsersDto.setLastName(orgUsers.getLastname());
		orgUsersDto.setOrgUserId(orgUsers.getId());
		orgUsersDto.setRoleDesc(orgRoles.getDescription());
		orgUsersDto.setOrderWorkflow(orderWorkflowService.getOrderWorkflow(orgRoles.getName()));

		return orgUsersDto;
	}

	@Override
	public String getOrgRoleName(int orgUsersId) {
		OrgUsersRoles orgUsersRoles = orgUsersRolesRepository.findOneByOrgUsersId(orgUsersId);
		OrgRoles orgRoles = orgRolesRepository.getOne(orgUsersRoles.getOrgRolesId());
		return orgRoles.getName();
	}

	@Override
	public int getOrgRoleDetailId(int orgUsersId) {
		OrgUsersRoles orgUsersRoles = orgUsersRolesRepository.findOneByOrgUsersId(orgUsersId);
		return orgUsersRoles.getRoleDetailId();
	}

	@Override
	public OrgUsers updatePassword(String email, String newpassword) {
		OrgUsers orgUsers = orgUsersRepository.findOneByEmail(email);
		orgUsers.setPwd(newpassword);
		orgUsersRepository.save(orgUsers);
		return orgUsers;
	}

	@Override
	public OrgUsers createOrgusers(OrgUsers orgUsers) {
		OrgUsers orgUsersNew = orgUsersRepository.save(orgUsers);
		return orgUsersNew;
	}

	@Override
	public OrgUsers checkUserAlreadyExists(OrgUsers orgUsers) {
		OrgUsers orgUsersEx = orgUsersRepository.findOneByEmail(orgUsers.getEmail()); //as
		if (orgUsersEx == null) {
			return null;
		} else {
			return orgUsersEx;
		}

	}

}
