package com.cgs.lms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cgs.lms.domain.OrderWorkflow;
import com.cgs.lms.domain.OrgRoles;
import com.cgs.lms.domain.OrgUsersRoles;
import com.cgs.lms.domain.repository.OrderWorkflowRepository;
import com.cgs.lms.domain.repository.OrgRolesRepository;
import com.cgs.lms.domain.repository.OrgUsersRepository;
import com.cgs.lms.domain.repository.OrgUsersRolesRepository;
import com.cgs.lms.service.OrderWorkflowService;

@Component("OrderWorkflowServiceImpl")
public class OrderWorkflowServiceImpl implements OrderWorkflowService {

	@Autowired
	OrgUsersRepository orgUsersRepository;

	@Autowired
	OrgRolesRepository orgRolesRepository;

	@Autowired
	OrgUsersRolesRepository orgUsersRolesRepository;

	@Autowired
	OrderWorkflowRepository orderOrderWorkflowRepository;

	@Override
	public List<OrderWorkflow> getOrderWorkflow(String role) {
		return orderOrderWorkflowRepository.findAllByRoleAccessOrderByStatusSeq(role);
	}

	@Override
	public List<OrderWorkflow> getOrderWorkflow(int userId) {
		OrgUsersRoles orgUsersRoles = orgUsersRolesRepository.findOneByOrgUsersId(userId);
		OrgRoles orgRoles = orgRolesRepository.getOne(orgUsersRoles.getOrgRolesId());
		String roleName = orgRoles.getName();
		return getOrderWorkflow(roleName);
	}

}
