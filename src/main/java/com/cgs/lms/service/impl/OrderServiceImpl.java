package com.cgs.lms.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cgs.lms.constants.AppConstants;
import com.cgs.lms.domain.ItemImages;
import com.cgs.lms.domain.OrderAddresses;
import com.cgs.lms.domain.OrderItems;
import com.cgs.lms.domain.Orders;
import com.cgs.lms.domain.repository.ItemImageRepository;
import com.cgs.lms.domain.repository.LocationsRepository;
import com.cgs.lms.domain.repository.OrderAddressesRepository;
import com.cgs.lms.domain.repository.OrderItemRepository;
import com.cgs.lms.domain.repository.OrderRepository;
import com.cgs.lms.dto.OrderItemsDto;
import com.cgs.lms.dto.OrdersDto;
import com.cgs.lms.mapper.OrdersItemsMapper;
import com.cgs.lms.mapper.OrdersMapper;
import com.cgs.lms.service.OrderService;
import com.cgs.lms.service.UsersService;

@Component("OrderServiceImpl")
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	OrderItemRepository orderItemRepository;

	@Autowired
	LocationsRepository locationsRepository;

	@Autowired
	OrderAddressesRepository orderAddressesRepository;

	@Autowired
	@Qualifier("UsersServiceImpl")
	private UsersService usersService;

	@Inject
	private OrdersMapper ordersMapper;

	@Inject
	private OrdersItemsMapper orderItemsMapper;

	@Autowired
	ItemImageRepository itemImageRepository;

	@Override
	public List<OrdersDto> orders(int truckId, String status) {
		return getDriverOrders(truckId, status);
	}

	@Override
	public Orders updateOrder(int order_id, String order_status) {
		Orders orders = orderRepository.findOne(order_id);
		orders.setOrderStatus(order_status);
		orderRepository.save(orders);
		return orders;
	}

	@Override
	public List<OrdersDto> orders(String orgUserId, String status) {
		String roleName = usersService.getOrgRoleName(NumberUtils.toInt(orgUserId));
		int roleDetailId = usersService.getOrgRoleDetailId(NumberUtils.toInt(orgUserId));
		List<OrdersDto> orders = null;
		switch (roleName) {
		case AppConstants.Roles.Driver:
			orders = getDriverOrders(roleDetailId, status);
			break;
		default:
			orders = getWarehouseOrders(roleDetailId, status);
		}
		return orders;
	}

	public List<OrdersDto> getDriverOrders(int truckId, String status) {

		List<Orders> orders = orderRepository.findAllByTruckIdAndOrderStatusAndOrderTypeOrderByIdAsc(truckId,
				status.trim(), AppConstants.OrderType.Delivery);
		List<OrdersDto> ordersDtos = prepareOrdersDto(orders);
		return ordersDtos;
	}

	public List<OrdersDto> getWarehouseOrders(int locationId, String status) {
		List<Orders> orders = orderRepository.findAllByLocationIdAndOrderStatusAndOrderTypeOrderByIdAsc(locationId,
				status.trim(), AppConstants.OrderType.Delivery);
		List<OrdersDto> ordersDtos = prepareOrdersDto(orders);
		return ordersDtos;
	}

	private List<OrdersDto> prepareOrdersDto(List<Orders> orders) {
		List<OrdersDto> ordersDtos = ordersMapper.orderToOrderDtoList(orders);
		for (OrdersDto ordersDto : ordersDtos) {
			System.out.println("ordersDto.getId()=" + ordersDto.getId());
			// Order Items
			List<OrderItems> orderItems = orderItemRepository.findAllByOrderId(ordersDto.getId());
			List<OrderItemsDto> orderItemsDtos = orderItemsMapper.orderItemToOrdervDtoList(orderItems);
			for (OrderItemsDto orderItemsDto : orderItemsDtos) {
				System.out.println("iten_id=" + orderItemsDto.getItemId());
				ItemImages itemImages = itemImageRepository.findOneByItemId(orderItemsDto.getItemId());
				if (itemImages != null) {
					String imageUrl = itemImageRepository.findOneByItemId(orderItemsDto.getItemId())
							.getRackspaceCdnUrlThumb();
					orderItemsDto.setItemImage(imageUrl);
				}
			}
			ordersDto.setOrderItems(orderItemsDtos);
			// location
			ordersDto.setLocations(locationsRepository.findOneByLocationId(ordersDto.getLocationId()));
			// order Address
			List<OrderAddresses> orderAddresses = orderAddressesRepository.findAllByOrderId(ordersDto.getId());
			if (orderAddresses.size() > 0) {
				ordersDto.setOrderAddresses(orderAddresses.get(0));
			}

		}
		return ordersDtos;

	}

	@Override
	public Orders updateOrder(int order_id, String order_status, int truckId) {
		Orders orders = orderRepository.findOne(order_id);
		orders.setOrderStatus(order_status);
		orders.setTruckId(truckId);
		orderRepository.save(orders);
		return orders;
	
	}

}
