package com.cgs.lms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cgs.lms.domain.OrgTrucks;
import com.cgs.lms.domain.OrgUsers;
import com.cgs.lms.domain.Trucks;
import com.cgs.lms.domain.repository.OrgTrucksRepository;
import com.cgs.lms.domain.repository.TrucksRepository;
import com.cgs.lms.service.TruckService;

@Component("TruckServiceImpl")
public class TruckServiceImpl implements TruckService {

	@Autowired
	TrucksRepository trucksRepository;

	@Autowired
	OrgTrucksRepository orgTrucksRepository;

	@Override
	public Trucks login(int truckId, String password) {
		return trucksRepository.findOneByIdAndPassword(truckId, password);
	}

	@Override
	public Trucks updatePassword(int truckId, String newpassword) {
		Trucks trucks = trucksRepository.findOne(truckId);
		trucks.setPassword(newpassword);
		return trucks;
	}

	@Override
	public OrgTrucks checkOrgTrucksAlreadyExists(OrgTrucks orgUsers) {
		OrgTrucks orgUsersEx = orgTrucksRepository.findOneByName(orgUsers.getName());
		if (orgUsersEx == null) {
			return null;
		} else {
			return orgUsersEx;
		}

	}

	@Override
	public OrgTrucks createOrgTrucks(OrgTrucks orgUsers) {
		OrgTrucks orgUsersNew = orgTrucksRepository.save(orgUsers);
		return orgUsersNew;
	}

	@Override
	public List<OrgTrucks> findAll() {
		return orgTrucksRepository.findAll();
	}

	@Override
	public List<OrgTrucks> findAllByLocationId(int locationId) {
		return orgTrucksRepository.findAllByLocationId(locationId);
	}

}
