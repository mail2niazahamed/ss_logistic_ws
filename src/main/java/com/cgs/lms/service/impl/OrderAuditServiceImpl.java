package com.cgs.lms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cgs.lms.domain.OrderAudit;
import com.cgs.lms.domain.repository.OrderAuditRepository;
import com.cgs.lms.service.OrderAuditService;

@Component("OrderAuditServiceImpl")
public class OrderAuditServiceImpl implements OrderAuditService {

	@Autowired
	OrderAuditRepository orderAuditRepository;

	@Override
	public List<OrderAudit> getOrderAudit(int order_id) {
		return orderAuditRepository.findAllByOrderId(order_id);
	}

}
