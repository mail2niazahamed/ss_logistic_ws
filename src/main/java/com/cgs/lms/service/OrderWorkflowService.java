package com.cgs.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cgs.lms.domain.OrderWorkflow;

@Service
public interface OrderWorkflowService {

	public List<OrderWorkflow> getOrderWorkflow(String role);

	public List<OrderWorkflow> getOrderWorkflow(int userId);

}
