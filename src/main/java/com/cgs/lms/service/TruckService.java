package com.cgs.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cgs.lms.domain.OrgTrucks;
import com.cgs.lms.domain.Trucks;

@Service
public interface TruckService {

	public Trucks login(int truckId, String password);

	public Trucks updatePassword(int truckId, String newpassword);

	public OrgTrucks checkOrgTrucksAlreadyExists(OrgTrucks orgUsers);

	public OrgTrucks createOrgTrucks(OrgTrucks orgUsers);

	public List<OrgTrucks> findAll();

	public List<OrgTrucks> findAllByLocationId(int parseInt);

}
