package com.cgs.lms.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cgs.lms.domain.OrderAudit;
import com.cgs.lms.domain.OrderWorkflow;
import com.cgs.lms.domain.Orders;
import com.cgs.lms.domain.OrgTrucks;
import com.cgs.lms.domain.OrgUsers;
import com.cgs.lms.domain.Trucks;
import com.cgs.lms.domain.repository.ContactRepository;
import com.cgs.lms.domain.repository.CustomersRepository;
import com.cgs.lms.domain.repository.ItemRepository;
import com.cgs.lms.domain.repository.UserRepository;
import com.cgs.lms.dto.CustomersDto;
import com.cgs.lms.dto.LoginUsersDto;
import com.cgs.lms.dto.OrdersDto;
import com.cgs.lms.dto.TruckDto;
import com.cgs.lms.dto.UsersDto;
import com.cgs.lms.service.CustomersService;
import com.cgs.lms.service.OrderAuditService;
import com.cgs.lms.service.OrderService;
import com.cgs.lms.service.OrderWorkflowService;
import com.cgs.lms.service.TruckService;
import com.cgs.lms.service.UsersService;
import com.cgs.lms.util.HeaderUtil;

@RestController
public class LogisticController {

	@Autowired
	ContactRepository contactRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	CustomersRepository customersRepository;

	@Autowired
	@Qualifier("TruckServiceImpl")
	private TruckService truckService;

	@Autowired
	@Qualifier("OrderServiceImpl")
	private OrderService orderService;

	@Autowired
	@Qualifier("OrderAuditServiceImpl")
	private OrderAuditService orderAuditService;

	@Autowired
	@Qualifier("UsersServiceImpl")
	private UsersService usersService;

	@Autowired
	@Qualifier("CustomersServiceImpl")
	private CustomersService customersService;

	@Autowired
	@Qualifier("OrderWorkflowServiceImpl")
	private OrderWorkflowService orderWorkflowService;

	@RequestMapping(value = "/login_truck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TruckDto> loginForTruck(@RequestParam(value = "truckId") String truckId,
			@RequestParam(value = "password") String password) {
		if (NumberUtils.isNumber(truckId)) {
			Trucks trucks = truckService.login(Integer.parseInt(truckId.trim()), password.trim());
			if (trucks == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Login", "Please check your credentials", ""))
						.body(null);
			}
			UsersDto usersDto = usersService.getUser(trucks.getUserId());

			if (usersDto == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Login", "User does not exists", "")).body(null);
			}
			CustomersDto customersDto = customersService.getCustomersDto(usersDto.getCustomerId());
			TruckDto truckDto = new TruckDto();
			truckDto.setTrucks(trucks);
			truckDto.setUsers(usersDto);
			truckDto.setCustomers(customersDto);
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("Login", "Login Successfull")).body(truckDto);

		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Login", "TruckId should be a Number", "")).body(null);
		}

	}

	@RequestMapping(value = "/orders_truck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdersDto>> ordersForTruck(@RequestParam(value = "truckId") String truckId,
			@RequestParam(value = "status") String status) {
		if (NumberUtils.isNumber(truckId)) {
			List<OrdersDto> orders = orderService.orders(Integer.parseInt(truckId.trim()), status);
			if (orders == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Orders", "Order does not exists", "")).body(null);
			} else
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Orders", "Orders listing Successfull"))
						.body(orders);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "TruckId should be a Number ", "")).body(null);
		}

	}

	@RequestMapping(value = "/change_truck_password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Trucks> changeTruckPassword(@RequestParam(value = "newpassword") String newpassword,
			@RequestParam(value = "truckId") String truckId) {
		if (NumberUtils.isNumber(truckId)) {
			Trucks trucks = truckService.updatePassword(Integer.parseInt(truckId.trim()), newpassword);
			if (trucks == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Truck", "Truck does not exists", "")).body(null);
			} else
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Truck", "Password Changed Successfully !"))
						.body(trucks);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "TruckId should be a Number ", "")).body(null);
		}

	}

	@RequestMapping(value = "/order_workflow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderWorkflow>> getOrderWorkflow(@RequestParam(value = "role") String role) {
		List<OrderWorkflow> orderWorkflows = orderWorkflowService.getOrderWorkflow(role);
		if (orderWorkflows == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Truck", "No Workflow exists for your role", "")).body(null);
		} else
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("Truck", "Password Changed Successfully !"))
					.body(orderWorkflows);

	}

	@RequestMapping(value = "/update_order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Orders> updateOrder(@RequestParam(value = "order_id") String order_id,
			@RequestParam(value = "order_status") String order_status) {
		if (NumberUtils.isNumber(order_id)) {
			Orders orders = orderService.updateOrder(Integer.parseInt(order_id.trim()), order_status);
			if (orders == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Orders", "No order found with this Id", "")).body(null);
			} else
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Orders", "Order submitted Successfully !"))
						.body(orders);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "Order Id should be a Number ", "")).body(null);
		}

	}

	@RequestMapping(value = "/order_audit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderAudit>> getOrderAudit(@RequestParam(value = "order_id") String order_id) {
		if (NumberUtils.isNumber(order_id)) {
			List<OrderAudit> orderAudits = orderAuditService.getOrderAudit(Integer.parseInt(order_id.trim()));
			if (orderAudits == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Orders", "No order found with this Id", "")).body(null);
			} else
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Orders", "Order submitted Successfully !"))
						.body(orderAudits);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "Order Id should be a Number ", "")).body(null);
		}

	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoginUsersDto> login(@RequestParam(value = "email") String email,
			@RequestParam(value = "password") String password) {
		LoginUsersDto orgUsersDto = usersService.authenticate(email.trim(), password.trim());
		if (orgUsersDto == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Login", "Please check your credentials", "")).body(null);
		}

		return ResponseEntity.ok().headers(HeaderUtil.createAlert("Login", "Login Successfull")).body(orgUsersDto);

	}

	@RequestMapping(value = "/orders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdersDto>> orders(@RequestParam(value = "orgUserId") String orgUserId,
			@RequestParam(value = "status") String status) {
		if (NumberUtils.isNumber(orgUserId)) {
			List<OrdersDto> ordersDtos = orderService.orders(orgUserId, status);

			return ResponseEntity.ok().headers(HeaderUtil.createAlert("Orders", "Orders listing Successfull"))
					.body(ordersDtos);

		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "Id should be a Number ", "")).body(null);
		}

	}

	@RequestMapping(value = "/change_password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrgUsers> changePassword(@RequestParam(value = "newpassword") String newpassword,
			@RequestParam(value = "email") String email) {
		OrgUsers orgUsers = usersService.updatePassword(email, newpassword);
		if (orgUsers == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Truck", "Truck does not exists", "")).body(null);
		} else
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("Truck", "Password Changed Successfully !"))
					.body(orgUsers);

	}

	@RequestMapping(value = "/create_orgusers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrgUsers> createOrgusers(@Valid @RequestBody OrgUsers orgUsers, HttpServletRequest request) {
		OrgUsers orgUsersEx = usersService.checkUserAlreadyExists(orgUsers);
		if (orgUsersEx != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("User", "User already exists", ""))
					.body(null);
		}
		OrgUsers orgUsersNew = usersService.createOrgusers(orgUsers);
		if (orgUsersNew == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("User", "Something went wrong. Please contact Admin", ""))
					.body(null);
		} else
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("User", "User created Successfully !"))
					.body(orgUsersNew);
	}

	@RequestMapping(value = "/modify_orgusers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrgUsers> modifyOrgusers(@Valid @RequestBody OrgUsers orgUsers, HttpServletRequest request) {
		OrgUsers orgUsersEx = usersService.checkUserAlreadyExists(orgUsers);
		if (orgUsersEx == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("User", "No user exists with this email", "")).body(null);
		}
		OrgUsers orgUsersNew = usersService.createOrgusers(orgUsers);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("User", "User modified Successfully !"))
				.body(orgUsersNew);
	}

	@RequestMapping(value = "/create_orgtrucks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrgTrucks> createOrgtrucks(@Valid @RequestBody OrgTrucks orgUsers,
			HttpServletRequest request) {
		OrgTrucks orgUsersEx = truckService.checkOrgTrucksAlreadyExists(orgUsers);
		if (orgUsersEx != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("User", "User already exists", ""))
					.body(null);
		}
		OrgTrucks orgUsersNew = truckService.createOrgTrucks(orgUsers);
		if (orgUsersNew == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("User", "Something went wrong. Please contact Admin", ""))
					.body(null);
		} else
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("User", "User created Successfully !"))
					.body(orgUsersNew);
	}

	@RequestMapping(value = "/modify_orgtrucks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrgTrucks> modifyOrgtrucks(@Valid @RequestBody OrgTrucks orgUsers,
			HttpServletRequest request) {
		OrgTrucks orgUsersEx = truckService.checkOrgTrucksAlreadyExists(orgUsers);
		if (orgUsersEx == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("User", "No user exists with this email", "")).body(null);
		}
		OrgTrucks orgUsersNew = truckService.createOrgTrucks(orgUsers);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("User", "User modified Successfully !"))
				.body(orgUsersNew);
	}

	@RequestMapping(value = "/org_trucks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrgTrucks>> getOrgtrucks(@RequestParam(value = "location_id") String location_id) {
		if (NumberUtils.isNumber(location_id)) {
			List<OrgTrucks> orgUsersEx = truckService.findAllByLocationId(Integer.parseInt(location_id.trim()));
			if (orgUsersEx == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Org Trucks", "No Org Truck exists", "")).body(null);
			} else {
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Org Trucks", "")).body(orgUsersEx);
			}
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Org Trucks", "Location Id should be a Number ", ""))
					.body(null);
		}
	}

	@RequestMapping(value = "/update_order_truck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Orders> updateOrderWithTruck(@RequestParam(value = "order_id") String order_id,
			@RequestParam(value = "order_status") String order_status,
			@RequestParam(value = "truck_id") String truck_id) {
		if (NumberUtils.isNumber(order_id) || NumberUtils.isNumber(truck_id)) {
			Orders orders = orderService.updateOrder(Integer.parseInt(order_id.trim()), order_status,
					Integer.parseInt(truck_id.trim()));
			if (orders == null) {
				return ResponseEntity.badRequest()
						.headers(HeaderUtil.createFailureAlert("Orders", "No order found with this Id", "")).body(null);
			} else
				return ResponseEntity.ok().headers(HeaderUtil.createAlert("Orders", "Order submitted Successfully !"))
						.body(orders);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("Orders", "Order Id should be a Number ", "")).body(null);
		}

	}
}
