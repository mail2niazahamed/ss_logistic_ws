package com.cgs.lms.constants;

public class AppConstants {

	public static interface OrderStatus {
		public static final String New = "E";
	}
	
	public static interface OrderType {
		public static final String Delivery = "D";
		public static final String WillCall = "W";
		public static final String online = "O";
	}
	
	public static interface Roles {
		public static final String Driver = "DRIVER";
		public static final String WhManager = "WAREHOUSE_MANAGER";
		public static final String WhDelManager = "WAREHOUSE_DELIVERY";
		public static final String WhDriver = "WH_DRIVER";
	}
	
}
