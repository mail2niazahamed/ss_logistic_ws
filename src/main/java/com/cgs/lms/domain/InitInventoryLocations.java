package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * InitInventoryLocations generated by hbm2java
 */
@Entity
@Table(name = "init_inventory_locations", schema = "public")
public class InitInventoryLocations implements java.io.Serializable {

	private InitInventoryLocationsId id;

	public InitInventoryLocations() {
	}

	public InitInventoryLocations(InitInventoryLocationsId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "locationId", column = @Column(name = "location_id")),
			@AttributeOverride(name = "qtyOnHand", column = @Column(name = "qty_on_hand", scale = 9)),
			@AttributeOverride(name = "qtyInProcess", column = @Column(name = "qty_in_process", scale = 9)),
			@AttributeOverride(name = "dateCreated", column = @Column(name = "date_created", length = 29)),
			@AttributeOverride(name = "dateLastModified", column = @Column(name = "date_last_modified", length = 29)),
			@AttributeOverride(name = "lastMaintainedBy", column = @Column(name = "last_maintained_by", length = 30)),
			@AttributeOverride(name = "companyId", column = @Column(name = "company_id", length = 8)),
			@AttributeOverride(name = "lastRecPo", column = @Column(name = "last_rec_po", scale = 9)),
			@AttributeOverride(name = "lastRecPoWithDisc", column = @Column(name = "last_rec_po_with_disc", scale = 9)),
			@AttributeOverride(name = "glAccountNo", column = @Column(name = "gl_account_no", length = 32)),
			@AttributeOverride(name = "purchOrTransfer", column = @Column(name = "purch_or_transfer", length = 1)),
			@AttributeOverride(name = "nextDueInPoCost", column = @Column(name = "next_due_in_po_cost", scale = 9)),
			@AttributeOverride(name = "nextDueInPoDate", column = @Column(name = "next_due_in_po_date", length = 29)),
			@AttributeOverride(name = "revenueAccountNo", column = @Column(name = "revenue_account_no", length = 32)),
			@AttributeOverride(name = "cosAccountNo", column = @Column(name = "cos_account_no", length = 32)),
			@AttributeOverride(name = "sellable", column = @Column(name = "sellable", length = 1)),
			@AttributeOverride(name = "movingAverageCost", column = @Column(name = "moving_average_cost", scale = 9)),
			@AttributeOverride(name = "standardCost", column = @Column(name = "standard_cost", scale = 9)),
			@AttributeOverride(name = "protectedStockQty", column = @Column(name = "protected_stock_qty", scale = 9)),
			@AttributeOverride(name = "invMin", column = @Column(name = "inv_min", scale = 9)),
			@AttributeOverride(name = "invMax", column = @Column(name = "inv_max", scale = 9)),
			@AttributeOverride(name = "safetyStock", column = @Column(name = "safety_stock", scale = 9)),
			@AttributeOverride(name = "stockable", column = @Column(name = "stockable", length = 1)),
			@AttributeOverride(name = "replenishmentLocation", column = @Column(name = "replenishment_location")),
			@AttributeOverride(name = "monthsInSeason", column = @Column(name = "months_in_season")),
			@AttributeOverride(name = "averageMonthlyUsage", column = @Column(name = "average_monthly_usage")),
			@AttributeOverride(name = "productGroupId", column = @Column(name = "product_group_id", length = 8)),
			@AttributeOverride(name = "purchaseClass", column = @Column(name = "purchase_class", length = 8)),
			@AttributeOverride(name = "cycleCountingCategory", column = @Column(name = "cycle_counting_category")),
			@AttributeOverride(name = "purchaseDiscountGroup", column = @Column(name = "purchase_discount_group", length = 8)),
			@AttributeOverride(name = "salesDiscountGroup", column = @Column(name = "sales_discount_group", length = 8)),
			@AttributeOverride(name = "noCharge", column = @Column(name = "no_charge", length = 1)),
			@AttributeOverride(name = "price1", column = @Column(name = "price1", scale = 9)),
			@AttributeOverride(name = "price2", column = @Column(name = "price2", scale = 9)),
			@AttributeOverride(name = "price3", column = @Column(name = "price3", scale = 9)),
			@AttributeOverride(name = "price4", column = @Column(name = "price4", scale = 9)),
			@AttributeOverride(name = "price5", column = @Column(name = "price5", scale = 9)),
			@AttributeOverride(name = "price6", column = @Column(name = "price6", scale = 9)),
			@AttributeOverride(name = "price7", column = @Column(name = "price7", scale = 9)),
			@AttributeOverride(name = "price8", column = @Column(name = "price8", scale = 9)),
			@AttributeOverride(name = "price9", column = @Column(name = "price9", scale = 9)),
			@AttributeOverride(name = "price10", column = @Column(name = "price10", scale = 9)),
			@AttributeOverride(name = "replenishmentMethod", column = @Column(name = "replenishment_method", length = 8)),
			@AttributeOverride(name = "orderQuantity", column = @Column(name = "order_quantity", scale = 9)),
			@AttributeOverride(name = "qtyAllocated", column = @Column(name = "qty_allocated", scale = 9)),
			@AttributeOverride(name = "qtyBackordered", column = @Column(name = "qty_backordered", scale = 9)),
			@AttributeOverride(name = "qtyInTransit", column = @Column(name = "qty_in_transit", scale = 9)),
			@AttributeOverride(name = "trackBins", column = @Column(name = "track_bins", length = 1)),
			@AttributeOverride(name = "defaultInOe", column = @Column(name = "default_in_oe", length = 1)),
			@AttributeOverride(name = "periodFirstStocked", column = @Column(name = "period_first_stocked")),
			@AttributeOverride(name = "yearFirstStocked", column = @Column(name = "year_first_stocked")),
			@AttributeOverride(name = "usageLock", column = @Column(name = "usage_lock", length = 1)),
			@AttributeOverride(name = "usageLockPeriod", column = @Column(name = "usage_lock_period")),
			@AttributeOverride(name = "usageLockYear", column = @Column(name = "usage_lock_year")),
			@AttributeOverride(name = "primaryBin", column = @Column(name = "primary_bin", length = 10)),
			@AttributeOverride(name = "taxGroupId", column = @Column(name = "tax_group_id", length = 10)),
			@AttributeOverride(name = "qtyReservedDueIn", column = @Column(name = "qty_reserved_due_in", scale = 9)),
			@AttributeOverride(name = "dateLastCounted", column = @Column(name = "date_last_counted", length = 29)),
			@AttributeOverride(name = "invMastUid", column = @Column(name = "inv_mast_uid")),
			@AttributeOverride(name = "requisition", column = @Column(name = "requisition", length = 1)),
			@AttributeOverride(name = "deadstockFlag", column = @Column(name = "deadstock_flag", length = 1)),
			@AttributeOverride(name = "lotBinIntegration", column = @Column(name = "lot_bin_integration", length = 1)),
			@AttributeOverride(name = "addToCycleCount", column = @Column(name = "add_to_cycle_count", length = 1)),
			@AttributeOverride(name = "defaultShipment", column = @Column(name = "default_shipment")),
			@AttributeOverride(name = "primarySupplierId", column = @Column(name = "primary_supplier_id")),
			@AttributeOverride(name = "onObtFlag", column = @Column(name = "on_obt_flag", length = 1)),
			@AttributeOverride(name = "onReleaseScheduleFlag", column = @Column(name = "on_release_schedule_flag", length = 1)),
			@AttributeOverride(name = "onBackorderFlag", column = @Column(name = "on_backorder_flag", length = 1)),
			@AttributeOverride(name = "lastSaleDate", column = @Column(name = "last_sale_date", length = 29)),
			@AttributeOverride(name = "lastPurchaseDate", column = @Column(name = "last_purchase_date", length = 29)),
			@AttributeOverride(name = "buy", column = @Column(name = "buy", length = 1)),
			@AttributeOverride(name = "make", column = @Column(name = "make", length = 1)),
			@AttributeOverride(name = "createdBy", column = @Column(name = "created_by")),
			@AttributeOverride(name = "periodsToSupplyMin", column = @Column(name = "periods_to_supply_min")),
			@AttributeOverride(name = "periodsToSupplyMax", column = @Column(name = "periods_to_supply_max")),
			@AttributeOverride(name = "putawayRank", column = @Column(name = "putaway_rank", length = 8)),
			@AttributeOverride(name = "ivaTaxableFlag", column = @Column(name = "iva_taxable_flag", length = 1)),
			@AttributeOverride(name = "invLastChangedDate", column = @Column(name = "inv_last_changed_date", length = 29)),
			@AttributeOverride(name = "itemPutawayAttributeUid", column = @Column(name = "item_putaway_attribute_uid")),
			@AttributeOverride(name = "splittableFlag", column = @Column(name = "splittable_flag", length = 1)),
			@AttributeOverride(name = "minReplenishmentQty", column = @Column(name = "min_replenishment_qty", scale = 9)),
			@AttributeOverride(name = "discontinued", column = @Column(name = "discontinued", length = 1)),
			@AttributeOverride(name = "allowDsDiscontinuedItems", column = @Column(name = "allow_ds_discontinued_items", length = 1)),
			@AttributeOverride(name = "allowSpDiscontinuedItems", column = @Column(name = "allow_sp_discontinued_items", length = 1)),
			@AttributeOverride(name = "safetyStockType", column = @Column(name = "safety_stock_type")),
			@AttributeOverride(name = "serviceLevelMeasure", column = @Column(name = "service_level_measure")),
			@AttributeOverride(name = "serviceLevelPctGoal", column = @Column(name = "service_level_pct_goal")),
			@AttributeOverride(name = "demandPatternCd", column = @Column(name = "demand_pattern_cd")),
			@AttributeOverride(name = "demandPatternEvaluationDate", column = @Column(name = "demand_pattern_evaluation_date", length = 29)),
			@AttributeOverride(name = "demandForecastFormulaUid", column = @Column(name = "demand_forecast_formula_uid")),
			@AttributeOverride(name = "patternManuallyEditedFlag", column = @Column(name = "pattern_manually_edited_flag", length = 1)),
			@AttributeOverride(name = "demandPatternBehaviorCd", column = @Column(name = "demand_pattern_behavior_cd")),
			@AttributeOverride(name = "patternLikeInvMastUid", column = @Column(name = "pattern_like_inv_mast_uid")),
			@AttributeOverride(name = "patternLikeLocationId", column = @Column(name = "pattern_like_location_id")),
			@AttributeOverride(name = "behavesLikeLockFlag", column = @Column(name = "behaves_like_lock_flag", length = 1)),
			@AttributeOverride(name = "behavesLikeLockPeriod", column = @Column(name = "behaves_like_lock_period")),
			@AttributeOverride(name = "behavesLikeLockYear", column = @Column(name = "behaves_like_lock_year")),
			@AttributeOverride(name = "landedCostAccountNo", column = @Column(name = "landed_cost_account_no")),
			@AttributeOverride(name = "priceFamilyUid", column = @Column(name = "price_family_uid")),
			@AttributeOverride(name = "deleteFlag", column = @Column(name = "delete_flag", length = 10)),
			@AttributeOverride(name = "defaultSellingUnit", column = @Column(name = "default_selling_unit")),
			@AttributeOverride(name = "cardlockProductId", column = @Column(name = "cardlock_product_id")),
			@AttributeOverride(name = "locItemTypeCd", column = @Column(name = "loc_item_type_cd")),
			@AttributeOverride(name = "locCustParentInvMastUid", column = @Column(name = "loc_cust_parent_inv_mast_uid")),
			@AttributeOverride(name = "mainBulkLocationFlag", column = @Column(name = "main_bulk_location_flag", length = 1)),
			@AttributeOverride(name = "dfltSourceLocFlag", column = @Column(name = "dflt_source_loc_flag", length = 1)),
			@AttributeOverride(name = "cycleCountFlag", column = @Column(name = "cycle_count_flag", length = 1)),
			@AttributeOverride(name = "promotionalFlag", column = @Column(name = "promotional_flag", length = 1)),
			@AttributeOverride(name = "rmaRevenueAccountNo", column = @Column(name = "rma_revenue_account_no", length = 32)),
			@AttributeOverride(name = "futureStandardCost", column = @Column(name = "future_standard_cost", scale = 9)),
			@AttributeOverride(name = "effectiveDate", column = @Column(name = "effective_date", length = 29)),
			@AttributeOverride(name = "assemblyPromptOption", column = @Column(name = "assembly_prompt_option")),
			@AttributeOverride(name = "restrictedFlag", column = @Column(name = "restricted_flag", length = 1)),
			@AttributeOverride(name = "drpItemFlag", column = @Column(name = "drp_item_flag", length = 1)),
			@AttributeOverride(name = "savedDemandForecastFormulaUid", column = @Column(name = "saved_demand_forecast_formula_uid")),
			@AttributeOverride(name = "demandFormulaComputedYearPeriod", column = @Column(name = "demand_formula_computed_year_period")),
			@AttributeOverride(name = "demandPatternComputedYearPeriod", column = @Column(name = "demand_pattern_computed_year_period")),
			@AttributeOverride(name = "yearPeriodLastForecast", column = @Column(name = "year_period_last_forecast")),
			@AttributeOverride(name = "vendorRebateAccountNo", column = @Column(name = "vendor_rebate_account_no", length = 32)),
			@AttributeOverride(name = "transferUsagePercent", column = @Column(name = "transfer_usage_percent")) })
	public InitInventoryLocationsId getId() {
		return this.id;
	}

	public void setId(InitInventoryLocationsId id) {
		this.id = id;
	}

}
