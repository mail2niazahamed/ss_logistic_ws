package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ItemsId generated by hbm2java
 */
@Embeddable
public class ItemsId implements java.io.Serializable {

	private Integer id;
	private String itemId;
	private String itemDesc;
	private String classId4;
	private String classId5;
	private String extendedDesc;
	private String shortCode;
	private String defaultSellingUnit;
	private String keywords;

	public ItemsId() {
	}

	public ItemsId(Integer id, String itemId, String itemDesc, String classId4, String classId5, String extendedDesc,
			String shortCode, String defaultSellingUnit, String keywords) {
		this.id = id;
		this.itemId = itemId;
		this.itemDesc = itemDesc;
		this.classId4 = classId4;
		this.classId5 = classId5;
		this.extendedDesc = extendedDesc;
		this.shortCode = shortCode;
		this.defaultSellingUnit = defaultSellingUnit;
		this.keywords = keywords;
	}

	@Column(name = "id", unique = true)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "item_id")
	public String getItemId() {
		return this.itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Column(name = "item_desc")
	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	@Column(name = "class_id4")
	public String getClassId4() {
		return this.classId4;
	}

	public void setClassId4(String classId4) {
		this.classId4 = classId4;
	}

	@Column(name = "class_id5")
	public String getClassId5() {
		return this.classId5;
	}

	public void setClassId5(String classId5) {
		this.classId5 = classId5;
	}

	@Column(name = "extended_desc")
	public String getExtendedDesc() {
		return this.extendedDesc;
	}

	public void setExtendedDesc(String extendedDesc) {
		this.extendedDesc = extendedDesc;
	}

	@Column(name = "short_code")
	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	@Column(name = "default_selling_unit")
	public String getDefaultSellingUnit() {
		return this.defaultSellingUnit;
	}

	public void setDefaultSellingUnit(String defaultSellingUnit) {
		this.defaultSellingUnit = defaultSellingUnit;
	}

	@Column(name = "keywords")
	public String getKeywords() {
		return this.keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ItemsId))
			return false;
		ItemsId castOther = (ItemsId) other;

		return ((this.getId() == castOther.getId())
				|| (this.getId() != null && castOther.getId() != null && this.getId().equals(castOther.getId())))
				&& ((this.getItemId() == castOther.getItemId()) || (this.getItemId() != null
						&& castOther.getItemId() != null && this.getItemId().equals(castOther.getItemId())))
				&& ((this.getItemDesc() == castOther.getItemDesc()) || (this.getItemDesc() != null
						&& castOther.getItemDesc() != null && this.getItemDesc().equals(castOther.getItemDesc())))
				&& ((this.getClassId4() == castOther.getClassId4()) || (this.getClassId4() != null
						&& castOther.getClassId4() != null && this.getClassId4().equals(castOther.getClassId4())))
				&& ((this.getClassId5() == castOther.getClassId5()) || (this.getClassId5() != null
						&& castOther.getClassId5() != null && this.getClassId5().equals(castOther.getClassId5())))
				&& ((this.getExtendedDesc() == castOther.getExtendedDesc())
						|| (this.getExtendedDesc() != null && castOther.getExtendedDesc() != null
								&& this.getExtendedDesc().equals(castOther.getExtendedDesc())))
				&& ((this.getShortCode() == castOther.getShortCode()) || (this.getShortCode() != null
						&& castOther.getShortCode() != null && this.getShortCode().equals(castOther.getShortCode())))
				&& ((this.getDefaultSellingUnit() == castOther.getDefaultSellingUnit())
						|| (this.getDefaultSellingUnit() != null && castOther.getDefaultSellingUnit() != null
								&& this.getDefaultSellingUnit().equals(castOther.getDefaultSellingUnit())))
				&& ((this.getKeywords() == castOther.getKeywords()) || (this.getKeywords() != null
						&& castOther.getKeywords() != null && this.getKeywords().equals(castOther.getKeywords())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result + (getItemId() == null ? 0 : this.getItemId().hashCode());
		result = 37 * result + (getItemDesc() == null ? 0 : this.getItemDesc().hashCode());
		result = 37 * result + (getClassId4() == null ? 0 : this.getClassId4().hashCode());
		result = 37 * result + (getClassId5() == null ? 0 : this.getClassId5().hashCode());
		result = 37 * result + (getExtendedDesc() == null ? 0 : this.getExtendedDesc().hashCode());
		result = 37 * result + (getShortCode() == null ? 0 : this.getShortCode().hashCode());
		result = 37 * result + (getDefaultSellingUnit() == null ? 0 : this.getDefaultSellingUnit().hashCode());
		result = 37 * result + (getKeywords() == null ? 0 : this.getKeywords().hashCode());
		return result;
	}

}
