package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Advertisements generated by hbm2java
 */
@Entity
@Table(name = "advertisements", schema = "public")
public class Advertisements implements java.io.Serializable {

	private int id;
	private String name;
	private String description;
	private String imageIphone;
	private Integer customerId;
	private Date begDate;
	private Date endDate;
	private String rackspaceCdnUrl;
	private String rackspaceCdnUrlIphone;
	private String rackspaceCdnUrlIphoneTh;
	private Date createdAt;
	private Date updatedAt;
	private String rackspaceCdnUrlAsmall;
	private String rackspaceCdnUrlAsmallTh;
	private String rackspaceCdnUrlAlarge;
	private String rackspaceCdnUrlAlargeTh;
	private String imageAsmall;
	private String imageAlarge;
	private String adUrl;

	public Advertisements() {
	}

	public Advertisements(int id) {
		this.id = id;
	}

	public Advertisements(int id, String name, String description, String imageIphone, Integer customerId, Date begDate,
			Date endDate, String rackspaceCdnUrl, String rackspaceCdnUrlIphone, String rackspaceCdnUrlIphoneTh,
			Date createdAt, Date updatedAt, String rackspaceCdnUrlAsmall, String rackspaceCdnUrlAsmallTh,
			String rackspaceCdnUrlAlarge, String rackspaceCdnUrlAlargeTh, String imageAsmall, String imageAlarge,
			String adUrl) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.imageIphone = imageIphone;
		this.customerId = customerId;
		this.begDate = begDate;
		this.endDate = endDate;
		this.rackspaceCdnUrl = rackspaceCdnUrl;
		this.rackspaceCdnUrlIphone = rackspaceCdnUrlIphone;
		this.rackspaceCdnUrlIphoneTh = rackspaceCdnUrlIphoneTh;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.rackspaceCdnUrlAsmall = rackspaceCdnUrlAsmall;
		this.rackspaceCdnUrlAsmallTh = rackspaceCdnUrlAsmallTh;
		this.rackspaceCdnUrlAlarge = rackspaceCdnUrlAlarge;
		this.rackspaceCdnUrlAlargeTh = rackspaceCdnUrlAlargeTh;
		this.imageAsmall = imageAsmall;
		this.imageAlarge = imageAlarge;
		this.adUrl = adUrl;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 1024)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "image_iphone")
	public String getImageIphone() {
		return this.imageIphone;
	}

	public void setImageIphone(String imageIphone) {
		this.imageIphone = imageIphone;
	}

	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "beg_date", length = 13)
	public Date getBegDate() {
		return this.begDate;
	}

	public void setBegDate(Date begDate) {
		this.begDate = begDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date", length = 13)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "rackspace_cdn_url")
	public String getRackspaceCdnUrl() {
		return this.rackspaceCdnUrl;
	}

	public void setRackspaceCdnUrl(String rackspaceCdnUrl) {
		this.rackspaceCdnUrl = rackspaceCdnUrl;
	}

	@Column(name = "rackspace_cdn_url_iphone")
	public String getRackspaceCdnUrlIphone() {
		return this.rackspaceCdnUrlIphone;
	}

	public void setRackspaceCdnUrlIphone(String rackspaceCdnUrlIphone) {
		this.rackspaceCdnUrlIphone = rackspaceCdnUrlIphone;
	}

	@Column(name = "rackspace_cdn_url_iphone_th")
	public String getRackspaceCdnUrlIphoneTh() {
		return this.rackspaceCdnUrlIphoneTh;
	}

	public void setRackspaceCdnUrlIphoneTh(String rackspaceCdnUrlIphoneTh) {
		this.rackspaceCdnUrlIphoneTh = rackspaceCdnUrlIphoneTh;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", length = 29)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "rackspace_cdn_url_asmall")
	public String getRackspaceCdnUrlAsmall() {
		return this.rackspaceCdnUrlAsmall;
	}

	public void setRackspaceCdnUrlAsmall(String rackspaceCdnUrlAsmall) {
		this.rackspaceCdnUrlAsmall = rackspaceCdnUrlAsmall;
	}

	@Column(name = "rackspace_cdn_url_asmall_th")
	public String getRackspaceCdnUrlAsmallTh() {
		return this.rackspaceCdnUrlAsmallTh;
	}

	public void setRackspaceCdnUrlAsmallTh(String rackspaceCdnUrlAsmallTh) {
		this.rackspaceCdnUrlAsmallTh = rackspaceCdnUrlAsmallTh;
	}

	@Column(name = "rackspace_cdn_url_alarge")
	public String getRackspaceCdnUrlAlarge() {
		return this.rackspaceCdnUrlAlarge;
	}

	public void setRackspaceCdnUrlAlarge(String rackspaceCdnUrlAlarge) {
		this.rackspaceCdnUrlAlarge = rackspaceCdnUrlAlarge;
	}

	@Column(name = "rackspace_cdn_url_alarge_th")
	public String getRackspaceCdnUrlAlargeTh() {
		return this.rackspaceCdnUrlAlargeTh;
	}

	public void setRackspaceCdnUrlAlargeTh(String rackspaceCdnUrlAlargeTh) {
		this.rackspaceCdnUrlAlargeTh = rackspaceCdnUrlAlargeTh;
	}

	@Column(name = "image_asmall")
	public String getImageAsmall() {
		return this.imageAsmall;
	}

	public void setImageAsmall(String imageAsmall) {
		this.imageAsmall = imageAsmall;
	}

	@Column(name = "image_alarge")
	public String getImageAlarge() {
		return this.imageAlarge;
	}

	public void setImageAlarge(String imageAlarge) {
		this.imageAlarge = imageAlarge;
	}

	@Column(name = "ad_url")
	public String getAdUrl() {
		return this.adUrl;
	}

	public void setAdUrl(String adUrl) {
		this.adUrl = adUrl;
	}

}
