package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Items generated by hbm2java
 */
@Entity
@Table(name = "items", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Items implements java.io.Serializable {

	private ItemsId id;

	public Items() {
	}

	public Items(ItemsId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", unique = true)),
			@AttributeOverride(name = "itemId", column = @Column(name = "item_id")),
			@AttributeOverride(name = "itemDesc", column = @Column(name = "item_desc")),
			@AttributeOverride(name = "classId4", column = @Column(name = "class_id4")),
			@AttributeOverride(name = "classId5", column = @Column(name = "class_id5")),
			@AttributeOverride(name = "extendedDesc", column = @Column(name = "extended_desc")),
			@AttributeOverride(name = "shortCode", column = @Column(name = "short_code")),
			@AttributeOverride(name = "defaultSellingUnit", column = @Column(name = "default_selling_unit")),
			@AttributeOverride(name = "keywords", column = @Column(name = "keywords")) })
	public ItemsId getId() {
		return this.id;
	}

	public void setId(ItemsId id) {
		this.id = id;
	}

}
