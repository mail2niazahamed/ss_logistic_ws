package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DataImportLogs generated by hbm2java
 */
@Entity
@Table(name = "data_import_logs", schema = "public")
public class DataImportLogs implements java.io.Serializable {

	private int id;
	private String filename;
	private String archivedFilename;
	private Date begDate;
	private Date endDate;
	private String result;
	private Date createdAt;
	private Date updatedAt;

	public DataImportLogs() {
	}

	public DataImportLogs(int id) {
		this.id = id;
	}

	public DataImportLogs(int id, String filename, String archivedFilename, Date begDate, Date endDate, String result,
			Date createdAt, Date updatedAt) {
		this.id = id;
		this.filename = filename;
		this.archivedFilename = archivedFilename;
		this.begDate = begDate;
		this.endDate = endDate;
		this.result = result;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "filename")
	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Column(name = "archived_filename")
	public String getArchivedFilename() {
		return this.archivedFilename;
	}

	public void setArchivedFilename(String archivedFilename) {
		this.archivedFilename = archivedFilename;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "beg_date", length = 29)
	public Date getBegDate() {
		return this.begDate;
	}

	public void setBegDate(Date begDate) {
		this.begDate = begDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date", length = 29)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "result", length = 4000)
	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", length = 29)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
