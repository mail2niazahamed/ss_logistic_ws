package com.cgs.lms.domain;
// Generated Jun 27, 2016 5:46:29 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Orders generated by hbm2java
 */
@Entity
@Table(name = "orders", schema = "public")
public class Orders implements java.io.Serializable {

	private int id;
	private Integer customerId;
	private Integer userId;
	private Integer locationId;
	private String poNumber;
	private Integer truckId;
	private Date orderDate;
	private Date orderDueDate;
	private Date createdAt;
	private Date updatedAt;
	private String orderType;
	private String orderStatus;
	private boolean exported;
	private String orderNotes;

	public Orders() {
	}

	public Orders(int id, boolean exported) {
		this.id = id;
		this.exported = exported;
	}

	public Orders(int id, Integer customerId, Integer userId, Integer locationId, String poNumber, Integer truckId,
			Date orderDate, Date orderDueDate, Date createdAt, Date updatedAt, String orderType, String orderStatus,
			boolean exported, String orderNotes) {
		this.id = id;
		this.customerId = customerId;
		this.userId = userId;
		this.locationId = locationId;
		this.poNumber = poNumber;
		this.truckId = truckId;
		this.orderDate = orderDate;
		this.orderDueDate = orderDueDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.orderType = orderType;
		this.orderStatus = orderStatus;
		this.exported = exported;
		this.orderNotes = orderNotes;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "location_id")
	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	@Column(name = "po_number")
	public String getPoNumber() {
		return this.poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	@Column(name = "truck_id")
	public Integer getTruckId() {
		return this.truckId;
	}

	public void setTruckId(Integer truckId) {
		this.truckId = truckId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "order_date", length = 13)
	public Date getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_due_date", length = 29)
	public Date getOrderDueDate() {
		return this.orderDueDate;
	}

	public void setOrderDueDate(Date orderDueDate) {
		this.orderDueDate = orderDueDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", length = 29)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "order_type")
	public String getOrderType() {
		return this.orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Column(name = "order_status")
	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Column(name = "exported", nullable = false)
	public boolean isExported() {
		return this.exported;
	}

	public void setExported(boolean exported) {
		this.exported = exported;
	}

	@Column(name = "order_notes")
	public String getOrderNotes() {
		return this.orderNotes;
	}

	public void setOrderNotes(String orderNotes) {
		this.orderNotes = orderNotes;
	}

}
