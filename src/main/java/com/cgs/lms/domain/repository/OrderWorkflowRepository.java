package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrderWorkflow;

public interface OrderWorkflowRepository extends JpaRepository<OrderWorkflow, Integer> {

	List<OrderWorkflow> findAllByRoleAccessOrderByStatusSeq(String role);

}
