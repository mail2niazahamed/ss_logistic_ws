package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrderItems;
import com.cgs.lms.domain.Orders;

public interface OrderItemRepository extends JpaRepository<OrderItems, Integer> {

	List<OrderItems> findAllByOrderId(int id);


}
