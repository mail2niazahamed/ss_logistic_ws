package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cgs.lms.domain.Customers;
import com.cgs.lms.domain.CustomersId;

public interface CustomersRepository extends CrudRepository<Customers, CustomersId> {

	@Query("select c.id.name,c.id.address,c.id.city,c.id.zipCode,c.id.phone  from Customers c where c.id.id = :customerId ")
	List<Object[]> findCustomerIdById(@Param("customerId") Integer customerId);
}
