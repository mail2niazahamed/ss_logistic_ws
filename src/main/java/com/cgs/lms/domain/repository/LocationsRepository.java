package com.cgs.lms.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cgs.lms.domain.Locations;
import com.cgs.lms.domain.LocationsId;

public interface LocationsRepository extends JpaRepository<Locations, LocationsId> {

	@Query("select loc.id.name from Locations loc where loc.id.id = :locationId")
	String findOneByLocationId(@Param("locationId") Integer locationId);

}
