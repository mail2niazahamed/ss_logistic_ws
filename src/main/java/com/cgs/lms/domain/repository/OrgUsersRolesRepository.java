package com.cgs.lms.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrgUsersRoles;

public interface OrgUsersRolesRepository extends JpaRepository<OrgUsersRoles, Integer> {

	OrgUsersRoles findOneByOrgUsersId(int id);
}
