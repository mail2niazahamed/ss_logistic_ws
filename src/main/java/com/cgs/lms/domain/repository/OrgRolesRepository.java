package com.cgs.lms.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.OrgRoles;

public interface OrgRolesRepository extends JpaRepository<OrgRoles, Integer> {
}
