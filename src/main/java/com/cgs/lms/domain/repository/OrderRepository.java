package com.cgs.lms.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cgs.lms.domain.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

	List<Orders> findAllByTruckIdAndOrderStatus(int truckId, String status);

	List<Orders> findAllByTruckIdAndOrderStatusAndOrderTypeOrderByIdAsc(int truckId, String status, String delivery);

	List<Orders> findAllByLocationIdAndOrderStatusAndOrderTypeOrderByIdAsc(int locationId, String trim, String delivery);

}
