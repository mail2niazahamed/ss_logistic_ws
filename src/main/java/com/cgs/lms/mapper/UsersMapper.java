package com.cgs.lms.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.cgs.lms.domain.Users;
import com.cgs.lms.dto.UsersDto;

@Mapper(componentModel = "spring", uses = {})
public interface UsersMapper {

	UsersDto usersToUsersDto(Users users);

	List<UsersDto> usersToUsersDto(List<Users> users);

}
