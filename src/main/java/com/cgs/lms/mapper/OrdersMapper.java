package com.cgs.lms.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.cgs.lms.domain.Orders;
import com.cgs.lms.dto.OrdersDto;

@Mapper(componentModel = "spring", uses = {})
public interface OrdersMapper {

	OrdersDto orderToOrderDto(Orders order);

	List<OrdersDto> orderToOrderDtoList(List<Orders> order);
}
