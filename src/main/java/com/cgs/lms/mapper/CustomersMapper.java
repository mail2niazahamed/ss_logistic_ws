package com.cgs.lms.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.cgs.lms.domain.CustomersId;
import com.cgs.lms.dto.CustomersDto;

@Mapper(componentModel = "spring", uses = {})
public interface CustomersMapper {

	CustomersDto customersIdToCustomersDto(CustomersId customersId);
	
	CustomersId customersDtoToCustomerIds(CustomersDto customersDto);

	List<CustomersDto> customerIdsToCustomersDtoList(List<CustomersId> customersId);
}
