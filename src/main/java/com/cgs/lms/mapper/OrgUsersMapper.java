package com.cgs.lms.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.cgs.lms.domain.OrgUsers;
import com.cgs.lms.dto.OrgUsersDto;

@Mapper(componentModel = "spring", uses = {})
public interface OrgUsersMapper {

	OrgUsersDto orgUsersToOrgUsersDto(OrgUsers users);
	
	OrgUsers orgUsersDtoToOrgUsers(OrgUsersDto users);

	List<OrgUsersDto> orgUsersToOrgUsersDto(List<OrgUsers> users);

}
