package com.cgs.lms.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.cgs.lms"})
@EnableJpaRepositories(basePackages = {"com.cgs.lms.domain.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
